const express = require('express');
const fs = require("fs");
const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        db.init().then((result) => {
            const readMessages = result.map((message) => {
                return new Promise((resolve, reject) => {
                    fs.readFile(message, (err, result) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(JSON.parse(result))
                    })
                })
            });
            return Promise.all(readMessages);


        }).then((result) => {
            res.send(result)
        })
    });

    router.post('/', (req, res) => {
        const message = req.body;
        db.addItem(message)

    });

    return router;
};

module.exports = createRouter;